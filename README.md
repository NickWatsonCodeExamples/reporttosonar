## Usage
```
USAGE: xcreportToSonar <xcresult> [-e <excluded-file-extension1> ...]

OPTIONS:
  --excluded, -e   Multiple file extensions which are excluded from processing
  --help           Display available options
```

## Example
Run the tool for a given `xcresult` archive, exclude `.h` and `.m` files from processing and save the result to `sonarqube-generic-coverage.xml`.
```
xcreportToSonar myproject.xcresult --excluded .h .m > sonarqube-generic-coverage.xml
or just
xcreportToSonar myproject.xcresult > sonarqube-generic-coverage.xml
```

The Xcode 11's `xcresult` archive can be found in the derived data folder:
```
DerivedData/YourProject/Logs/Test/*.xcresult
```
