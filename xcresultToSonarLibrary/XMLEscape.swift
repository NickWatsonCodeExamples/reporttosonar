//
//  XMLEscape.swift
//

import Foundation

/// Escapes text for XML
internal class XMLEscape {

	internal init() {}
	
	/// Escapes string by replacing illegal characters
	func escape(_ string: String) -> String {
		var result = string
		result = result.replacingOccurrences(of:"&", with: "&amp;")
		result = result.replacingOccurrences(of:"'", with: "&apos;")
		result = result.replacingOccurrences(of:"\"", with: "&quot;")
		result = result.replacingOccurrences(of:"<", with: "&lt;")
		result = result.replacingOccurrences(of:">", with: "&gt;")
		return result
	}
	
}
