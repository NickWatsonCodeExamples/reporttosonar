//
//  IO.swift
//

import Foundation

public class IO {
	
	public init() {}
	
	public enum OutputType {
		case error
		case standard
	}
	
	public func print(_ message: String, to: OutputType = .standard) {
		switch to {
		case .standard:
			Swift.print("\(message)")
		case .error:
			fputs("\(message)\n", stderr)
		}
	}
	
}
